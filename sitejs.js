// This is the JS code on the website for referencing

var INPUT_READY = false;
var SLEEP = false;
var ELEMENT_MAIN = $("html");
var ELEMENT_MAIN, ELEMENT_FEED, ELEMENT_TERMINAL, ELEMENT_CAPTURE;

$.fn.image = function (src, f) {
    return this.each(function () {
        var i = new Image();
        i.src = src;
        i.onload = f;
        this.appendChild(i);
    });
}

var last_cmd = [];
var last_cmd_index = 0;
var buffer = "";
var cursor = false;
var line_limit = 30;

function refresh() {

    rows = buffer.split("\n");
    if (rows.length > line_limit)
        rows = rows.slice(-line_limit);

    ELEMENT_TERMINAL.html(rows.join("\n"));

}

function cmd_buffer(i) {


    if (last_cmd_index < 0)
        last_cmd_index = 0;
    if (last_cmd_index > last_cmd.length - 1)
        last_cmd_index = last_cmd.length - 1;


    rows = buffer.split("\n");

    rows[rows.length - 1] = ">" + last_cmd[last_cmd_index].replace(/[^A-Za-z0-9./_ -]/, '');

    buffer = rows.join("\n");
    refresh();

    last_cmd_index += i;

}


function write_init(s, t) {

    SLEEP = true;
    rows = get_rows(s);

    write(0, rows, t);
}


function write(i, rows, t) {

    INPUT_READY = false;


    if (i > rows.length - 1) {

        buffer += ">";

        refresh();

        SLEEP = false;
        INPUT_READY = true;
        return;
    }


    buffer += rows[i] + "\n";

    refresh();

    setTimeout(function () {
        write(i + 1, rows, t)
    }, t);

}

function get_rows(s) {

    return s.split("\n");


}


function blink() {

    if (!cursor)
        $("#terminal>span#trailing").remove();
    else
        ELEMENT_TERMINAL.append("<span id='trailing'>_</span>");
    cursor = !cursor;

}

function ready() {


    buffer += ">";

    refresh();
    INPUT_READY = true;


}

// Connect
$(window).load(function () {


    ELEMENT_TERMINAL = $("#terminal");
    ELEMENT_FEED = $("#feed #content");
    ELEMENT_CAPTURE = $("#capture_input");

    setInterval(blink, 200);


    var request = $.ajax({
        url: "interface.php",
        type: "POST",
        dataType: "json",
        timeout: 8000,
        data: {evt: 'connect'}
    })
            .done(function (data) {

                var _data = data;

                buffer += "> " + _data['msg'] + "\n\n";

                if (_data['video']) {

                    ELEMENT_FEED.image(_data['video'], function () {

                    });

                }


            })
            .fail(function () {

                buffer += "> <em>Connection error, please retry</em>\n\n";

            })

            .always(function () {

                ready();

            });

});


ELEMENT_MAIN.keypress(function (event) {

    if (event.ctrlKey)
        return;

    if (!INPUT_READY||SLEEP)
        return;

    switch (event.which) {

        case 17:
            return;
            break;

        case 38:

        case 16 :

        case  8 :

        case 13 :

        default :

            buffer += String.fromCharCode(event.which).replace(/[^A-Za-z0-9./_ -]/, '');
            break;
    }

    refresh();

});

ELEMENT_MAIN.keydown(function (event) {

    if (!INPUT_READY||SLEEP)
        return;
    if (event.ctrlKey)
        return;


    ELEMENT_CAPTURE.val("");
    ELEMENT_CAPTURE.focus();

    console.log(event.which);

    switch (event.which) {

        case  8 :

            event.preventDefault();

            var prompt = buffer.length - buffer.lastIndexOf("\n>");

            if (prompt > 2) {
                buffer = buffer.substring(0, buffer.length - 1);

                refresh();
            }

            break;

        case 38 :

            cmd_buffer(-1);

            break;

        case 40 :

            cmd_buffer(1);

            break;


        case 13 :

            INPUT_READY = false;

            event.preventDefault();

            var rows = buffer.split("\n");

            buffer += "\n";

            if (rows[rows.length - 1] == ">" || rows[rows.length - 1] == "") {

                refresh();
                ready();

                return;
            }

            last_cmd.push(rows[rows.length - 1]);

            last_cmd_index = last_cmd.length - 1;

            request = $.ajax({
                url: "interface.php",
                type: "POST",
                dataType: "json",
                timeout: 3000,
                data: {cmd: rows[rows.length - 1]}
            })
                    .done(function (_data) {

                        if (_data['exit'] == 1)
                            window.history.back();

                        if (_data['cls'] == 1)
                            buffer = "";

                        if (_data['busy']) {
                            write_init(_data['msg'], parseInt(_data['busy']));
                        } else
                            buffer += "> " + _data['msg'] + "\n\n";

                        if (_data['code']) {

                            $('div#code').remove();
                            $('body').append('<div id="code">' + _data['code'] + '</div>');

                        }

                        if (_data['btx']) {

                            ELEMENT_FEED.html("");
                            ELEMENT_FEED.html(_data['btx']);

                        } else if (_data['video']) {

                            //console.log(_data['video']);

                            ELEMENT_FEED.html("");
                            ELEMENT_FEED.image(_data['video'], function () {

                            });

                        }

                        if (!_data['busy']) {
                            ready();
                        }

                        //console.log( "success" );
                    })
                    .fail(function () {
                        //console.log( "error" );
                        buffer += "> <em>Connection error, please retry</em>\n\n";
                        ready();

                    })

                    .always(function () {
                        //console.log( "complete" );
                        refresh();
                    });

            break;

    }
})
