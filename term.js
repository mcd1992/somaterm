var superagent = require("superagent");
var htmlents   = require('html-entities').AllHtmlEntities;
htmlents       = new htmlents() // I have no clue why the author of html-entities does this...

var filefolder = "./data" // Location to store downloaded files
var localtest  = false ? "127.0.0.1" : "somagame.com";
var rooturl    = "http://" + localtest + "/YuXIm5/pathos2_cnsl";
var headers    = {
	"Connection": "keep-alive",
	"Pragma": "no-cache",
	"Cache-Control": "no-cache",
	"Accept": "application/json, text/javascript, */*; q=0.01",
	"Origin": "http://somagame.com",
	"X-Requested-With": "XMLHttpRequest",
	"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36",
	"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
	"Referer": "http://somagame.com/YuXIm5/pathos2_cnsl/",
	"Accept-Encoding": "gzip, deflate",
	"Accept-Language": "en-US,en;q=0.8"
}

// Sends a command to the soma website
// Callback returns ( error, response_json, superagent_response )
function SendCommand( command, callback ) {
	if ( typeof(command) == "string" ) {
		command = {
			cmd: ">" + command.slice( 0, -1 ) // Remove newline
		}
	}

	superagent
		.post( rooturl + "/interface.php" )
		.set ( headers )
		.send( command )
		.end ( function( error, response ) {
			if ( error ) {
				if ( callback ) {
					callback( error, null, response );
				}
			}

			if ( callback ) {
				callback( null, response ? response.text : null, response );
			}
	} );
}

function ParseResponse( error, json, response ) {
	if ( error || !json ) {
		console.log( error, json );
		return
	}

	var output = JSON.parse( json );
	console.log( output && htmlents.decode(unescape(output.msg)) );
}

SendCommand( {evt: "connect"}, ParseResponse ); // Send initial connection

// Read from stdin and send commands
process.stdin.setEncoding( "utf8" );
process.stdin.on( "readable", function() {
	var line = process.stdin.read();
	if ( line !== null ) {
		SendCommand( line, ParseResponse );
	}
});

process.stdin.on( "end", function() { // EOF Received
	process.stdout.write( "Exiting..." );
} );
